<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Form</title>
</head>
<body>
	<%@ include file="menu.jsp" %>

   <c:if test="${ !empty nom }"><p><c:out value="Bonjour, vous vous appelez ${ nom }" /></p></c:if>
   <c:if test="${ !empty sessionScope.prenom && !empty sessionScope.nom }">
        <p>Vous �tes ${ sessionScope.prenom } ${ sessionScope.nom } !</p>
    </c:if>
   <p>Cookie :<c:out value="${ prenom }"></c:out></p>
   <form method="post" action="Form">
       <label for="nom">Nom : </label>
       <input type="text" name="nom" id="nom" />
       <label for="prenom">Pr�nom : </label>
       <input type="text" name="prenom" id="prenom" />
       
       <input type="submit" />
   </form>
 
</body>
</html>