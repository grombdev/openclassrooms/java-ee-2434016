<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<menu>
	<ul>
		<li><a href="/test/">Accueil</a></li>
		<li><a href="/test/Test">Bonjour</a></li>
		<li><a href="/test/Test?name=Jérôme">Bonjour Jérôme</a></li>
		<li><a href="/test/Form">Form</a></li>
		<li><a href="/test/Login">Login</a></li>
		<li><a href="/test/File">File</a></li>
		<li><a href="/test/Db">Database</a></li>
	</ul>
</menu>