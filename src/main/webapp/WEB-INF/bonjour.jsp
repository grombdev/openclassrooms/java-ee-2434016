<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="menu.jsp" %>
	<p>Bonjour ${ empty name ? "" : name } !!!</p>
	<p>
	<%
	String variable = (String) request.getAttribute("variable");
	out.println(variable);
	%>
	</p>
	<p>
	<%
		for (int i = 0; i < 20; i++) {
			out.println(i);
		}
	%>
	<p>${ noms[0] }</p>
	<p>${ author.lastname } ${ author.firstname }</p>
	<p>${ author.actif ? "actif" : "inactif" }</p>
	<div><c:out value="JSTL auto-include !"/></div>
	<div><c:out value="JSTL author name: ${ author.lastname }"/></div>
	<div>JSTL surname: <c:out value="${ surname }" default="No surname"/></div>
	<div>JSTL birthdate: <c:out value="${ birthdate }">No birthdate</c:out></div>
	<p><c:out value="<b>Injection</b>" escapeXml="false"/></p>
	<c:set var="pseudo" value="TheBest" scope="page"/>
	<c:set var="date" value="today" scope="request"/>
	<c:set var="role" value="Admin" scope="session"/>
	<c:set var="mode" value="dark" scope="application"/>
	<p><c:out value="${pseudo}"/></p>
	<c:set var="pseudo" scope="page">TheBest2</c:set>
	<div><c:out value="${pseudo}"/></div>
	<div><c:out value="${date}"/></div>
	<div<c:out value="${role}"/></div>
	<div><c:out value="${mode}"/></div>
	<c:set target="${ author }" property="firstname" value="Johnny"/>
	<p><c:out value="${author.firstname}"/></p>
	<c:remove var="pseudo" scope="page"/>
	<c:if test="${ 50 > 10 }" var="fiftysupten" scope="page">50 est sup�rieur � 10 : ${ fiftysupten }</c:if>
	<br/>
	<c:choose>
		<c:when test="${ 50 < 20 }">50 inf 20</c:when>
		<c:when test="${ 50 > 100 }">50 sup 100</c:when>
		<c:otherwise>default</c:otherwise>
	</c:choose>
	<br/>
	<c:forEach begin="0" end="10" step="1" var="i">
		<span><c:out value="${ i }"/></span>
	</c:forEach>
	<ul>
		<c:forEach items="${ titres }" var="titre" varStatus="status">
	    	<li>N�<c:out value="${ status.count }" /> : <c:out value="${ titre }" /> !</li>
		</c:forEach>
	</ul>
	<div>
		<c:forTokens var="morceau" items="Un �l�ment/Encore un autre �l�ment/Un dernier pour la route" delims="/ ">
		    <span>${ morceau }</span>
		</c:forTokens>
	</div>
</body>
</html>