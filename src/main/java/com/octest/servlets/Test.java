package com.octest.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import com.octest.beans.Author;

/**
 * Servlet implementation class Test
 */
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//response.setContentType("text/html");
		//response.setCharacterEncoding("UTF-8");
		//PrintWriter out = response.getWriter();
		//out.println("Bonjour !");

		String name = request.getParameter("name");
		request.setAttribute("name", name);
		String message = "Au revoir !";
		request.setAttribute("variable", message);
		String[] noms = { "Alfred", "Brice", "Charles", "Dominique" };
		request.setAttribute("noms", noms);
		Author author = new Author();
		author.setLastname("Doe");
		author.setFirstname("John");
		author.setActif(true);
		request.setAttribute("author", author);

		String[] titres = {"Foundation", "Les fleurs du mal", "Les fables de la fontaine"};
		request.setAttribute("titres", titres);

		this.getServletContext().getRequestDispatcher("/WEB-INF/bonjour.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
