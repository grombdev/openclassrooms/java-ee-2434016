package com.octest.servlets;

import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import com.octest.bdd.Noms;
import com.octest.beans.User;
import com.octest.dao.DaoException;
import com.octest.dao.DaoFactory;
import com.octest.dao.UserDao;

/**
 * Servlet implementation class Db
 */
public class Db extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private UserDao utilisateurDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Db() {
        super();
        // TODO Auto-generated constructor stub
    }
    

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.utilisateurDao = daoFactory.getUtilisateurDao();
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        //Noms tableNoms = new Noms();
        //request.setAttribute("utilisateurs", tableNoms.recupererUtilisateurs());

		try {
            request.setAttribute("utilisateurs", utilisateurDao.lister());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        }
		
        this.getServletContext().getRequestDispatcher("/WEB-INF/db.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        try {
			User utilisateur = new User();
	        utilisateur.setNom(request.getParameter("nom"));
	        utilisateur.setPrenom(request.getParameter("prenom"));
	        
	        //Noms tableNoms = new Noms();
	        //tableNoms.ajouterUtilisateur(utilisateur);
	        //request.setAttribute("utilisateurs", tableNoms.recupererUtilisateurs());
            
            utilisateurDao.ajouter(utilisateur);
            request.setAttribute("utilisateurs", utilisateurDao.lister());
        }
        catch (Exception e) {
            request.setAttribute("erreur", e.getMessage());
        }
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/db.jsp").forward(request, response);
	}

}
