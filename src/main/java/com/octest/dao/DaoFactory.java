package com.octest.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DaoFactory {
    private String url;
    private String username;
    private String password;

    DaoFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public static DaoFactory getInstance() { 
    	String DB_URL = System.getenv("DB_URL");
    	String DB_USER = System.getenv("DB_USER");
    	String DB_PWD = System.getenv("DB_PWD");
   
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        	Logger.getAnonymousLogger().info(e.toString());
        }


        DaoFactory instance = new DaoFactory(
        		DB_URL, DB_USER, DB_PWD);
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connexion = DriverManager.getConnection(url, username, password);
        connexion.setAutoCommit(false);
        return connexion;
    }

    // Récupération du Dao
    public UserDao getUtilisateurDao() {
        return new UserDaoImpl(this);
    }
}